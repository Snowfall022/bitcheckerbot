package main

import (
	"bitchecker/internal/btc/service"
	"bitchecker/internal/btc/tatum"
	"bitchecker/internal/config"
	"bitchecker/internal/log"
	"bitchecker/internal/tgbot"
	"bitchecker/internal/tgbot/handler"
)

func main() {
	conf := config.Resolve()
	logger, err := log.New(conf.Log)
	if err != nil {
		panic(err)
	}
	tat := tatum.New(conf.Tatum.Token)
	btc := service.New(tat, logger)

	bot := tgbot.New(conf.Tgbot.Token)
	cmdHandler := handler.New(bot, btc, logger)

	bot.Handle("/ping", cmdHandler.Ping)
	bot.Handle("/balance", cmdHandler.Balance)
	bot.Handle("/generate_wallet", cmdHandler.GenerateWallet)
	bot.Handle("/sendbtc", cmdHandler.SendBTC)

	logger.Info("starting...")
	bot.Start()
}
