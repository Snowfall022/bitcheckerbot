package tgbot

import (
	"gopkg.in/tucnak/telebot.v2"
	"time"
)

func New(token string) *telebot.Bot {
	bot, err := telebot.NewBot(telebot.Settings{
		Token:  token,
		Poller: &telebot.LongPoller{Timeout: 60 * time.Second},
	})

	if err != nil {
		panic(err)
	}

	return bot
}
