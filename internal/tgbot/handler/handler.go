package handler

import (
	"bitchecker/internal/btc/service"
	"bitchecker/internal/log"
	"fmt"
	"gopkg.in/tucnak/telebot.v2"
	"regexp"
	"strconv"
)

type handler struct {
	bot *telebot.Bot
	btc *service.BitcoinManager
	log *log.Logger
}

func New(bot *telebot.Bot, btc *service.BitcoinManager, log *log.Logger) *handler {
	return &handler{
		bot: bot,
		btc: btc,
		log: log,
	}
}

func (h *handler) Ping(m *telebot.Message) {
	_, _ = h.bot.Send(m.Sender, "pong")
}

func (h *handler) Balance(m *telebot.Message) {
	if m.Payload == "" {
		_, _ = h.bot.Send(m.Sender, "please define address")
		return
	}
	balance, err := h.btc.GetBalance(m.Payload)
	if err != nil {
		h.log.Warnf("error on get balance %v", err)
		_, _ = h.bot.Send(m.Sender, "error occurred")
		return
	}
	_, _ = h.bot.Send(m.Sender, fmt.Sprintf("%f BTC", balance))
}

func (h *handler) GenerateWallet(m *telebot.Message) {
	res, err := h.btc.NewWallet()
	if err != nil {
		h.log.Warnf("error on generate wallet %v", err)
		h.bot.Send(m.Sender, "error occurred")
		return
	}

	responseTemplate := "*address:* %s\n*mnemonic:* %s\n*private key:* %s"
	responseMessage := fmt.Sprintf(responseTemplate, res.Address, res.Mnemonic, res.PrivateKey)

	_, _ = h.bot.Send(m.Sender, responseMessage, &telebot.SendOptions{ParseMode: telebot.ModeMarkdown})
}

func (h *handler) SendBTC(m *telebot.Message) {
	r := regexp.MustCompile("(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)")
	res := r.FindStringSubmatch(m.Payload)

	if len(res) != 5 {
		h.bot.Send(m.Sender, "please use this format: /sendbtc from_address to_address private_key amount")
		return
	}

	fromAddr := res[1]
	toAddr := res[2]
	privateKey := res[3]
	amount := res[4]

	amountFloat, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		h.log.Warnf("error on parsing amount for sendbtc %v", err)
		h.bot.Send(m.Sender, "amount should be a number")
		return
	}

	txID, err := h.btc.SendBTC(fromAddr, toAddr, privateKey, amountFloat)
	if err != nil {
		h.log.Warnf("error on sendbtc %v", err)
		h.bot.Send(m.Sender, "error occurred")
		return
	}

	h.bot.Send(m.Sender, fmt.Sprintf("transaction hash (id): %s", txID))
}
