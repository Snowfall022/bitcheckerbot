package config

import (
	"bitchecker/internal/log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type config struct {
	Tgbot *tgbot    `yaml:"tgbot"`
	Tatum *tatum    `yaml:"tatum"`
	Log   *log.Conf `yaml:"log"`
}

type tgbot struct {
	Token string `yaml:"token"`
}

type tatum struct {
	Token string `yaml:"token"`
}

func Resolve() *config {
	data, err := ioutil.ReadFile("config.yml")
	if err != nil {
		panic(err)
	}

	c := new(config)
	err = yaml.Unmarshal(data, c)
	if err != nil {
		panic(err)
	}

	return c
}
