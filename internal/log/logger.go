package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

type Logger struct {
	*logrus.Logger
}

type Conf struct {
	Level    string `yaml:"level"`
	FilePath string `yaml:"filePath"`
}

func New(conf *Conf) (*Logger, error) {
	level, err := logrus.ParseLevel(conf.Level)
	if err != nil {
		return nil, err
	}

	f, err := os.OpenFile(conf.FilePath, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		fmt.Printf("error opening log file: %v", err)
	}

	mw := io.MultiWriter(f, os.Stdout)

	logger := logrus.New()
	logger.SetOutput(mw)
	logger.SetLevel(level)

	return &Logger{logger}, nil
}
