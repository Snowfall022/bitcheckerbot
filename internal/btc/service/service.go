package service

import (
	"bitchecker/internal/btc/tatum"
	"bitchecker/internal/log"
)

const DefaultIndex = 1

type BitcoinManager struct {
	t   *tatum.Client
	log *log.Logger
}

func New(client *tatum.Client, log *log.Logger) *BitcoinManager {
	return &BitcoinManager{
		t:   client,
		log: log,
	}
}

func (b *BitcoinManager) GetBalance(addr string) (float64, error) {
	apiResponse, err := b.t.GetBalance(addr)
	if err != nil {
		return 0, err
	}

	return apiResponse.Balance(), nil
}

type WalletResult struct {
	Mnemonic   string
	PrivateKey string
	Address    string
}

func (b *BitcoinManager) NewWallet() (*WalletResult, error) {
	walletResponse, err := b.t.GenerateWallet()
	if err != nil {
		return nil, err
	}

	addressResponse, err := b.t.GenerateAddress(walletResponse.Xpub, DefaultIndex)
	if err != nil {
		return nil, err
	}

	privateKeyResponse, err := b.t.GeneratePrivateKey(walletResponse.Mnemonic, DefaultIndex)
	if err != nil {
		return nil, err
	}

	b.log.Info("new wallet created: ")

	return &WalletResult{
		Mnemonic:   walletResponse.Mnemonic,
		PrivateKey: privateKeyResponse.Key,
		Address:    addressResponse.Address,
	}, nil
}

func (b *BitcoinManager) SendBTC(from string, to string, privateKey string, amount float64) (string, error) {
	response, err := b.t.SendBTC(from, to, privateKey, amount)
	if err != nil {
		return "", err
	}

	return response.TxID, nil
}
