package tatum

import (
	"errors"
	"fmt"
	"github.com/dghubble/sling"
)

const BaseTatumUrl = "https://api-eu1.tatum.io"

type Client struct {
	token string
	base  *sling.Sling
}

func New(token string) *Client {
	return &Client{
		token: token,
		base:  sling.New().Base(BaseTatumUrl),
	}
}

type apiErrorResponse struct {
	StatusCode int         `json:"statusCode"`
	ErrorType  string      `json:"errorCode"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

func (a *apiErrorResponse) AsError() error {
	if a.StatusCode == 0 {
		return nil
	}

	return errors.New(fmt.Sprintf("%#v", *a))
}

func resultError(request error, response *apiErrorResponse) error {
	if request != nil {
		return request
	}

	return response.AsError()
}
