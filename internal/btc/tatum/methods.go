package tatum

import (
	"fmt"
	"strconv"
)

type getBalanceResponse struct {
	Incoming string `json:"incoming"`
	Outgoing string `json:"outgoing"`
}

func (r getBalanceResponse) getIncoming() float64 {
	incoming, _ := strconv.ParseFloat(r.Incoming, 64)
	return incoming
}

func (r getBalanceResponse) getOutgoing() float64 {
	outgoing, _ := strconv.ParseFloat(r.Outgoing, 64)
	return outgoing
}

func (r getBalanceResponse) Balance() float64 {
	return r.getIncoming() - r.getOutgoing()
}

func (c *Client) GetBalance(address string) (*getBalanceResponse, error) {
	path := fmt.Sprintf("/v3/bitcoin/address/balance/%s", address)
	apiResponse := new(getBalanceResponse)
	apiError := new(apiErrorResponse)

	_, err := c.base.
		Get(path).
		Set("X-Api-Key", c.token).
		Receive(apiResponse, apiError)

	return apiResponse, resultError(err, apiError)
}

type generateWalletResponse struct {
	Mnemonic string `json:"mnemonic"`
	Xpub     string `json:"xpub"`
}

func (c *Client) GenerateWallet() (*generateWalletResponse, error) {
	apiResponse := new(generateWalletResponse)
	apiError := new(apiErrorResponse)

	_, err := c.base.
		Get("/v3/bitcoin/wallet").
		Set("X-Api-Key", c.token).
		Receive(apiResponse, apiError)

	return apiResponse, resultError(err, apiError)
}

type generateAddressResponse struct {
	Address string `json:"address"`
}

func (c *Client) GenerateAddress(xpub string, index int) (*generateAddressResponse, error) {
	path := fmt.Sprintf("/v3/bitcoin/address/%s/%d", xpub, index)
	apiResponse := new(generateAddressResponse)
	apiError := new(apiErrorResponse)

	_, err := c.base.
		Get(path).
		Set("X-Api-Key", c.token).
		Receive(apiResponse, apiError)

	return apiResponse, resultError(err, apiError)
}

type generatePrivateKeyResponse struct {
	Key string `json:"key"`
}

type generatePrivateKeyRequest struct {
	Index    int    `json:"index"`
	Mnemonic string `json:"mnemonic"`
}

func (c *Client) GeneratePrivateKey(mnemonic string, index int) (*generatePrivateKeyResponse, error) {
	apiResponse := new(generatePrivateKeyResponse)
	apiError := new(apiErrorResponse)

	_, err := c.base.
		Post("/v3/bitcoin/wallet/priv").
		Set("X-Api-Key", c.token).
		BodyJSON(&generatePrivateKeyRequest{
			Index:    index,
			Mnemonic: mnemonic,
		}).
		Receive(apiResponse, apiError)

	return apiResponse, resultError(err, apiError)
}

type fromAddress struct {
	Address    string `json:"address"`
	PrivateKey string `json:"privateKey"`
}

type toAddress struct {
	Address string  `json:"address"`
	Value   float64 `json:"value"`
}

type sendBTCRequest struct {
	FromAddress []fromAddress `json:"fromAddress"`
	To          []toAddress   `json:"to"`
}

type sendBTCResponse struct {
	TxID   string `json:"txId"`
	Failed bool   `json:"failed"`
}

func (c *Client) SendBTC(from string, to string, privateKey string, value float64) (*sendBTCResponse, error) {
	apiResponse := new(sendBTCResponse)
	apiError := new(apiErrorResponse)

	_, err := c.base.
		Post("/v3/bitcoin/transaction").
		Set("X-Api-Key", c.token).
		BodyJSON(&sendBTCRequest{
			FromAddress: []fromAddress{{
				Address:    from,
				PrivateKey: privateKey,
			}},
			To: []toAddress{{
				Address: to, Value: value,
			}},
		}).
		Receive(apiResponse, apiError)

	return apiResponse, resultError(err, apiError)
}
